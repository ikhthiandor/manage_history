var path = require('path');
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');

module.exports = {
    'context': __dirname,

    'entry': './history_app/assets/js/index',

    'output': {
        'path': path.resolve('./history_app/assets/bundles/'),
        'filename': '[name]-[hash].js',
    },

    'plugins': [
        new BundleTracker({
            'filename': './webpack-stats.json'
        })
    ],

    'module': {
        'loaders': [
            {
                'test': '/\.jsx?$/',
                'exclude': '/(node_modules)/',
                'loader': 'babel',
                'query': {
                    'cacheDirectory': true,
                    'presets': ['react', 'es2015', 'stage-2']
                }
            }
        ]

    },

    'resolve': {
        'modulesDirectories': [
            'node_modules'
        ],
        'extensions': [
            '',
            '.js',
            '.jsx'
        ]
    }



}
