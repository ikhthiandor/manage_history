
from django.conf import settings

from django.views.generic.base import TemplateView



from react.render import render_component

import arrow
import os

# Create your views here.


HISTORY_FILE_PATH = '/home/ikhthiandor/.local/share/qutebrowser/history_without_empty_lines'

class HistoryView(TemplateView):

    template_name = 'index.html'


    def __init__(self):

        list_of_history_items = []

        with open(HISTORY_FILE_PATH, 'r') as history_source:

        # Read all history from file
            all_history = history_source.readlines()

            history_first_twenty_visits = all_history[0:21]

            stripped_newlines_history = [row.rstrip() for row in history_first_twenty_visits]

            for individual_item in stripped_newlines_history:

                visit_time_and_url = individual_item.split(' ')

                visit_timestamp = visit_time_and_url[0]

                # humanized_timestamp = arrow.get(visit_time_and_url[0]).format('MMMM DD, YYYY')

                visited_url = visit_time_and_url[1]

                # list_of_history_items.append({humanized_timestamp: visited_url})

                # list_of_history_items.append({humanized_timestamp: visited_url})

                list_of_history_items.append({visit_timestamp: visited_url})
            print(list_of_history_items)

            # final_output = humanized_timestamp + " " + visited_url
        self.list_of_histories = list_of_history_items

    def get_context_data(self, **kwargs):
        context = super(HistoryView, self).get_context_data(**kwargs)
        context['rendered'] = render_component(
            path=os.path.join(settings.BASE_DIR, 'history_app/assets/js/app.jsx'
            ),

            props={
                'historyItems': self.list_of_histories
            },

            to_static_markup=True

        )

        return context
