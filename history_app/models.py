from django.db import models

# Create your models here.

class History(models.Model):
    timestamp = models.DateTimeField()
    url = models.CharField(max_length=100)
