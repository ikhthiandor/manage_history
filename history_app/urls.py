from django.conf.urls import url
from history_app.views import HistoryView

urlpatterns = [
    url(r'^history/$', HistoryView.as_view())
]
