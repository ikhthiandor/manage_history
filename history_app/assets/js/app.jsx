import React from 'react';

import Item from './item.jsx';

var uuid = require('node-uuid');

class App extends React.Component {

    render() {

        var history_items = this.props.historyItems;
        // console.log( Object.keys(history_items[0]));

        var history_nodes = history_items.map(function(item) {
            // console.log(Object.values(item));

            var h_time = Object.keys(item);
            var h_url = item[h_time];

            return (
                <Item time={h_time} url={h_url} key={uuid.v1()}/>
            );
        });


        return (
            <div>
              {history_nodes}
            </div>

            );
      }

    }




export default App;
