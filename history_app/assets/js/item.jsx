import React from 'react';

class Item extends React.Component {
    render() {
        return (

            <li>Visited {this.props.url} at: {this.props.time}</li>

            );
        }

}

export default Item;
